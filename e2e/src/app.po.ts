import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get(browser.baseUrl) as Promise<any>;
  }

  getTitleText() {
    return element(by.css('app-root h1')).getText() as Promise<string>;
  }

  getBodyForm() {
    return element(by.css('app-root form')).getText() as Promise<string>;
  }

  getBodyTable() {
    return element(by.css('app-root table')).getText() as Promise<string>;
  }
}
