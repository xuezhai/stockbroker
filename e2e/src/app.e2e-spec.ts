import { AppPage } from './app.po';
import { browser, logging } from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getTitleText()).toEqual('Welcome to stockbroker-app');
  });

  it('should display stock form', () => {
    page.navigateTo();
    expect(page.getBodyForm()).toContain('Symbol');
  });

  it('should display stock hitorical quote table', () => {
    page.navigateTo();
    expect(page.getBodyTable()).toContain('Open');
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
