import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Utility } from './app.utility';

@Injectable()
export class DataService {
    //stock quote data from https://www.alphavantage.co/
    defaultUrl: string = 'https://www.alphavantage.co/query?function=';
    urlPostfix: string = '&apikey=LZALI2690ZUJIXO9&outputsize=compact';

    TIME_SERIES_MONTHLY: string = 'TIME_SERIES_MONTHLY';
    TIME_SERIES_WEEKLY: string = 'TIME_SERIES_WEEKLY';
    TIME_SERIES_DAILY: string = 'TIME_SERIES_DAILY';
    TIME_SERIES_INTRADAY: string = 'TIME_SERIES_INTRADAY';
    INTERVAL: string = '&interval=60min';

    WEEK: number = 7;
    MONTH: number = 30;

    constructor(private http: HttpClient) {
    }

    getData(symbol: string, startDate: string, endDate: string) {
        var timeSeries = this.getTimeSeries(startDate, endDate)
        var restAPIUrl = this.defaultUrl + timeSeries + '&symbol=' + symbol + this.urlPostfix;
        return this.http.get(restAPIUrl);
    }   

    getTimeSeries(startDate: string, endDate: string) {
        var diffDays = Utility.getDays(startDate, endDate);

        if (diffDays <= this.WEEK) {
            return this.TIME_SERIES_INTRADAY + this.INTERVAL;
        }
        else if (diffDays <= this.MONTH) {
            return this.TIME_SERIES_DAILY;
        }
        else {
            return this.TIME_SERIES_DAILY;
        }
    }
}