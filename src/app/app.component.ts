import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { StockBrokerValidator } from './app.validator';
import { DataService } from './app.dataservice';
import { Quote } from './app.stockquote';
import { Utility } from './app.utility';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'stockbroker-app';

  stockform: FormGroup;
  symbol = new FormControl("", Validators.required);

  tickerHistData: any = {};
  tableHistData = [];
  maxSize: number = 100;
  minSize: number = 3;

  constructor(fb: FormBuilder, private dataService: DataService) {
    this.stockform = fb.group({
      symbol: ["tmus", StockBrokerValidator.tickerFormat],
      startDate: ["01/01/2018", StockBrokerValidator.startDate],
      endDate: ["01/01/2019", StockBrokerValidator.endDate]
    });
  }

  ngOnInit() {
    this.dataService.getData('TMUS', '03/01/2018', '03/09/2019').subscribe(res => {
      this.tickerHistData = res;
      this.processDataForForm(this.maxSize);
    });
  }

  processDataForForm(dateRange: number) {
    this.tableHistData = [];
    var rowSize: number = 0;
    var totalSize = 0;

    if (dateRange >= this.maxSize) {
      totalSize = this.maxSize;
    }
    else {
      if (dateRange <= this.minSize) {
        totalSize = this.minSize;
      }
      else {
        totalSize = dateRange;
      }
    }

    for (var key in this.tickerHistData) {
      if (key.indexOf('Meta Data') >= 0) {
        this.symbol = this.tickerHistData[key]['2. Symbol'];
      }

      if (key.indexOf('Time Series') >= 0) {
        for (var subkey in this.tickerHistData[key]) {
          var quote: Quote = new Quote(subkey,
            this.tickerHistData[key][subkey]['1. open'],
            this.tickerHistData[key][subkey]['2. high'],
            this.tickerHistData[key][subkey]['3. low'],
            this.tickerHistData[key][subkey]['4. close'],
            this.tickerHistData[key][subkey]['5. volume']
          );
          if (rowSize <= totalSize) {
            this.tableHistData.push(quote);
            rowSize++;
          } else {
            break;
          }
        }
      }
    }
  }

  onSubmit() {
    console.log(" stock histry data for " + this.stockform.value.symbol + " submitted");
    this.dataService.getData(this.stockform.value.symbol, this.stockform.value.startDate, this.stockform.value.endDate)
      .subscribe(res => {
        this.tickerHistData = res;
        var dateRange = Utility.getDays(this.stockform.value.startDate, this.stockform.value.endDate);
        this.processDataForForm(dateRange);
      });
  }
}