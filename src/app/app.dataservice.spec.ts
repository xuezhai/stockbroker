import { TestBed, inject } from '@angular/core/testing';
import { HttpEvent, HttpEventType } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { DataService } from './app.dataservice';

describe('DataService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [DataService]
        });
    });

    it('should be initialized',
        inject(
            [HttpTestingController, DataService],
            (httpMock: HttpTestingController, dataService: DataService) => {
                expect(dataService).toBeTruthy();
            }));

    it('should get stock quote in week',
        inject(
            [HttpTestingController, DataService],
            (httpMock: HttpTestingController, dataService: DataService) => {

                dataService.getData("cost", "03/10/2019", "3/15/2019").subscribe((event: HttpEvent<any>) => {
                    switch (event.type) {
                        case HttpEventType.Response:
                            expect(event.body).toContain("COST");
                    }
                });
            }
        )
    );

    it('should get stock quote in month',
        inject(
            [HttpTestingController, DataService],
            (httpMock: HttpTestingController, dataService: DataService) => {

                dataService.getData("nflx", "02/18/2019", "3/04/2019").subscribe((event: HttpEvent<any>) => {
                    switch (event.type) {
                        case HttpEventType.Response:
                            expect(event.body).toContain("NFLX");
                    }
                });
            }
        )
    );

    it('should get stock quote longer than month',
        inject(
            [HttpTestingController, DataService],
            (httpMock: HttpTestingController, dataService: DataService) => {

                dataService.getData("goog", "02/18/2018", "3/04/2019").subscribe((event: HttpEvent<any>) => {
                    switch (event.type) {
                        case HttpEventType.Response:
                            expect(event.body).toContain("GOOG");
                    }
                });
            }
        )
    );

    it('should get no stock quote for invalid symbol',
        inject(
            [HttpTestingController, DataService],
            (httpMock: HttpTestingController, dataService: DataService) => {

                dataService.getData("XSLD", "02/18/2018", "3/04/2019").subscribe((event: HttpEvent<any>) => {
                    switch (event.type) {
                        case HttpEventType.Response:
                            expect(event.body).toContain("XSLD");
                    }
                });
            }
        )
    );
});