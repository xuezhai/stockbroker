//stock quote data definition
export class Quote {
    date: string;
    open: string;
    high: string;
    low: string;
    close: string;
    volume: string;

    constructor(date: string, open: string, high: string, low: string, close: string, volume: string) {
        this.date = date;
        this.open = open;
        this.high = high;
        this.low = low;
        this.close = close;
        this.volume = volume;
    }
}