import { FormControl } from '@angular/forms';

export class StockBrokerValidator {

    static startDate(control: FormControl): { [key: string]: any } {
        let datePattern = /^02\/(?:[01]\d|2\d)\/(?:19|20)(?:0[048]|[13579][26]|[2468][048])|(?:0[13578]|10|12)\/(?:[0-2]\d|3[01])\/(?:19|20)\d{2}|(?:0[469]|11)\/(?:[0-2]\d|30)\/(?:19|20)\d{2}|02\/(?:[0-1]\d|2[0-8])\/(?:19|20)\d{2}$/;

        if (!control.value.match(datePattern))
            return { "startDate": true };

        return null;
    }

    static endDate(control: FormControl): { [key: string]: any } {
        let datePattern = /^02\/(?:[01]\d|2\d)\/(?:19|20)(?:0[048]|[13579][26]|[2468][048])|(?:0[13578]|10|12)\/(?:[0-2]\d|3[01])\/(?:19|20)\d{2}|(?:0[469]|11)\/(?:[0-2]\d|30)\/(?:19|20)\d{2}|02\/(?:[0-1]\d|2[0-8])\/(?:19|20)\d{2}$/;

        if (!control.value.match(datePattern))
            return { "endDate": true };

        return null;
    }

    static tickerFormat(control: FormControl): { [key: string]: any } {
        let tickerPattern = /^[A-Za-z]{1,5}$/;

        if (!control.value.match(tickerPattern))
            return { "symbol": true };

        return null;
    }
}