//utility helper functions
export class Utility {
    static getDays(sDate: string, eDate: string){
        var date1 = new Date(sDate);
        var date2 = new Date(eDate);

        var timeDiff = Math.abs(date2.getTime() - date1.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

        return diffDays;
    }
}