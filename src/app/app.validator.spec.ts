import { StockBrokerValidator } from './app.validator';
import { FormControl } from '@angular/forms';

describe("StockBrokerValidator", () => {

    it("should correctly validate date as valid", () => {
        const formControl = new FormControl('03/04/1992');

        expect(StockBrokerValidator.startDate(formControl)).toBeNull;
        expect(StockBrokerValidator.endDate(formControl)).toBeNull;
    });

    it("should correctly validate date with invalid date char as invalid", () => {
        const formControl = new FormControl('ab/03/2018');

        expect(StockBrokerValidator.startDate(formControl)).toEqual({ "startDate": true });
        expect(StockBrokerValidator.endDate(formControl)).toEqual({ "endDate": true });
    });

    it("should correctly validate date with invalid month as invalid", () => {
        const formControl = new FormControl('19-68-2018');

        expect(StockBrokerValidator.startDate(formControl)).toEqual({ "startDate": true });
        expect(StockBrokerValidator.endDate(formControl)).toEqual({ "endDate": true });
    });

    it("should correctly validate date with invalid year, as invalid", () => {
        const formControl = new FormControl('12-01-018');

        expect(StockBrokerValidator.startDate(formControl)).toEqual({ "startDate": true });
        expect(StockBrokerValidator.endDate(formControl)).toEqual({ "endDate": true });
    });
});